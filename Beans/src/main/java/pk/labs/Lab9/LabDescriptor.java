package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = ?;
    public static Class<? extends Consultation> consultationBean = ?;
    public static Class<? extends ConsultationList> consultationListBean = ?;
    public static Class<? extends ConsultationListFactory> consultationListFactory = ?;
    
}
